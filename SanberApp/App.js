import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import Component from './Tugas/Tugas13/LoginScreen'
// import Component from './Tugas/Tugas13/RegisterScreen'
// import Component from './Tugas/Tugas13/AboutScreen'
// import Component from './Tugas/Tugas14/App'
import Component from './Tugas/Tugas15/index'

export default function App() {
  return (
    <Component />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
