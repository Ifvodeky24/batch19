import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { createDrawerNavigator } from "@react-navigation/drawer"
import { AuthContext } from "./context"

import { SignIn, CreateAccount, Home, Search, Details, Search2, Profile, Splash } from "./Screen"

const AuthStack = createStackNavigator()
const Tabs = createBottomTabNavigator()
const HomeStack = createStackNavigator()
const SearchStack = createStackNavigator()
const ProfileStack = createStackNavigator()
const RootStack = createStackNavigator()
const Drawer = createDrawerNavigator()

const AuthStackScreen = () => (
  <AuthStack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
    <AuthStack.Screen
      name="Sign In"
      component={SignIn}
      options={{ title: 'Sign In' }} />
    <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title: 'Create Account' }} />
  </AuthStack.Navigator>
)

const HomeStackScreen = () => (
  <HomeStack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
    <HomeStack.Screen name="Home" component={Home} />
    <HomeStack.Screen name="Details" component={Details} options={({ route }) => ({
      title: route.params.name
    })} />
  </HomeStack.Navigator>
)

const SearchStackScreen = () => (
  <SearchStack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
    <SearchStack.Screen name="Search" component={Search} />
    <SearchStack.Screen name="Search2" component={Search2} />
  </SearchStack.Navigator>
)

const ProfileStackScreen = () => (
  <ProfileStack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
)

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={HomeStackScreen} />
    <Tabs.Screen name="Search" component={SearchStackScreen} />
  </Tabs.Navigator>
)

const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Profile">
    <Drawer.Screen name="Home" component={TabsScreen} />
    <Drawer.Screen name="Profile" component={ProfileStackScreen} />
  </Drawer.Navigator>
)

const RootStackScreen = ({ userToken }) => (
  <RootStack.Navigator headerMode="none">

    {userToken ? (
      <RootStack.Screen name="Auth" component={AuthStackScreen} />
    ) : (
        <RootStack.Screen name="App" component={DrawerScreen} />
      )}

  </RootStack.Navigator>
)

export default () => {
  const [isLoading, setIsLoading] = React.useState(true)
  const [userToken, setUserToken] = React.useState(null)

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false)
        setUserToken('asdf')
      },
      signUp: () => {
        setIsLoading(false)
        setUserToken('asdf')
      },
      signOut: () => {
        setIsLoading(false)
        setUserToken(null)
      }
    }
  }, [])

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 1000)
  }, [])

  if (isLoading) {
    return <Splash />
  }


  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>

        {userToken ? (
          <Drawer.Navigator initialRouteName="Profile">
            <Drawer.Screen name="Home" component={TabsScreen} />
            <Drawer.Screen name="Profile" component={ProfileStackScreen} />
          </Drawer.Navigator>
        ) : (
            <AuthStack.Navigator screenOptions={{ headerTitleAlign: 'center' }}>
              <AuthStack.Screen
                name="Sign In"
                component={SignIn}
                options={{ title: 'Sign In' }} />
              <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{ title: 'Create Account' }} />
            </AuthStack.Navigator>
          )}

        {/* <RootStackScreen userToken={{userToken}} /> */}

      </NavigationContainer>
    </AuthContext.Provider >

  )
}

