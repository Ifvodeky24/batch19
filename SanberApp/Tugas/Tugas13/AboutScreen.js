import React, { Component } from "react";
import { View, StyleSheet, Image, TouchableOpacity, Text, FlatList, TextInput, Dimensions, Button } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'
import Icons from 'react-native-vector-icons/AntDesign'

const { width: WIDTH } = Dimensions.get('window')

export default class AboutScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.titleStyle}>Tentang Saya</Text>

                    <Icon name={'account-circle'} size={130} color={'#003366'}
                        style={styles.iconStyle}></Icon>

                    <Text style={styles.nameStyle}>Ifvo Deky Wirawan</Text>
                    <Text style={styles.jobStyle}>Android Developer</Text>
                </View>

                <View style={styles.portofolio}>
                    <Text style={styles.portofolioStyle}>Portofolio</Text>
                    <Icons name={'github'} size={40} color={'#003366'}
                        style={styles.iconStyle}></Icons>
                    <Text style={styles.jobStyle}>@ifvodeky24</Text>
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        padding: 20
    },
    headerContainer: {
        alignItems: 'center',
        marginTop: 25
    },
    titleStyle: {
        color: "#003366",
        justifyContent: 'center',
        fontSize: 24,
        fontWeight: 'bold'
    },
    nameStyle: {
        color: "#003366",
        justifyContent: 'center',
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 8
    },
    jobStyle: {
        color: "#3EC6FF",
        justifyContent: 'center',
        fontSize: 14,
        marginTop: 8
    },
    iconStyle: {
        justifyContent: 'center',
        top: 12,
        marginBottom: 8
    },
    portofolio: {
        height: 120,
        backgroundColor: "#EFEFEF",
        padding: 8,
        marginTop: 12
    },
    portofolioStyle: {
        color: "#003366",
        justifyContent: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 2
    }
})  