import React, { Component } from "react";
import { View, StyleSheet, Image, TouchableOpacity, Text, FlatList, TextInput, Dimensions, Button } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'

import logo from './images/logo.png'

const { width: WIDTH } = Dimensions.get('window')


export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image source={logo} style={styles.logoStyle} />
                    <Text style={styles.logoText}>PORTOFOLIO</Text>
                </View>

                <View>
                    <Text style={styles.titleStyle}>Login</Text>
                </View>

                <View>
                    <Text style={styles.textInput}>Username</Text>
                </View>

                <View>

                    <Icon name={'account-circle'} size={28} color={'#003366'}
                        style={styles.iconStyle}></Icon>

                    <TextInput style={styles.input}
                        placeholder={'Username'}
                        placeholderTextColor={'#000000'}>
                    </TextInput>
                </View>

                <View>
                    <Text style={styles.textInput}>Email</Text>
                </View>

                <View>

                    <Icon name={'email'} size={28} color={'#003366'}
                        style={styles.iconStyle}></Icon>

                    <TextInput style={styles.input}
                        placeholder={'Email'}
                        placeholderTextColor={'#000000'}>
                    </TextInput>
                </View>

                <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.btnLogin} >
                        <Text style={styles.textLogin}>Masuk?</Text>
                    </TouchableOpacity>

                    <Text style={styles.textAtau}>Atau</Text>

                    <TouchableOpacity style={styles.btnRegister}>
                        <Text style={styles.textRegister}>Daftar</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        padding: 20
    },
    logoContainer: {
        alignItems: 'center'
    },
    logoStyle: {
        marginTop: 50,
        width: 350,
        height: 50
    },
    logoText: {
        color: "#3EC6FF",
        alignItems: 'flex-end'
    },
    titleStyle: {
        fontSize: 18,
        color: "#003366",
        marginTop: 22,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    textInput: {
        color: "#003366",
        fontSize: 12,
        marginTop: 20
    },
    input: {
        width: WIDTH - 55,
        height: 40,
        borderRadius: 10,
        fontSize: 16,
        paddingLeft: 45,
        borderWidth: 1,
        borderColor: "#003366",
        marginTop: 8
    },
    iconStyle: {
        position: 'absolute',
        top: 12,
        left: 10
    },

    btnLogin: {
        width: WIDTH - 55,
        height: 40,
        borderRadius: 10,
        fontSize: 16,
        borderWidth: 1,
        borderColor: "#003366",
        marginTop: 8,
        backgroundColor: '#3EC6FF',
        justifyContent: 'center'
    },
    textLogin: {
        color: 'white',
        textAlign: 'center'
    },
    textAtau: {
        marginTop : 5,
        color: '#3EC6FF',
        textAlign: 'center'
    },
    btnRegister: {
        width: WIDTH - 55,
        height: 40,
        borderRadius: 10,
        fontSize: 16,
        borderWidth: 1,
        borderColor: "#003366",
        marginTop: 8,
        backgroundColor: '#003366',
        justifyContent: 'center'
    },
    textRegister: {
        color: '#FFFFFF',
        textAlign: 'center'
    },
    btnContainer : {
        marginTop : 15
    }
})